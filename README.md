# Prisma Cloud Container scan in Concourse #

Simple Concourse pipeline which shows how to scan a container image in Concourse using twistcli.
The container image is built from the repo, then we proceed to download twistcli from the  Prisma Cloud Console (via API) and finally we do the scan.

